#include "stack.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <assert.h>



#define STACK_ASSERT(expr)   assert(expr)
#define STACK_UNLIMITED_SIZE ((size_t) 0)

#define CAST(type, value) ((type) (value))
#define CAST_PTR(x)       CAST(void*, x)
#define CAST_CONST_PTR(x) CAST(const void*, x)

/**
 * @brief Convert a @c TNode* to a @c Stack_ElementPtr .
 *
 * @param m_pNode A value of type @c TNode* .
 */
#define TO_STACK_ELEMENT_PTR(m_pNode) \
  CAST(Stack_ElementPtr, CAST(uintptr_t, m_pNode) + sizeof(TNode))

/**
 * @brief Convert a @c Stack_ElementPtr to a @c TNode* .
 *
 * @param m_pElement A value of type @c Stack_ElementPtr .
 */
#define TO_TNODE_PTR(m_pElement) CAST(TNode*, CAST(uintptr_t, m_pElement) - sizeof(TNode))

/***************** Declaration of Private Typedefs *****************/

typedef struct Node TNode;

/***************** Declaration of Private Structs *****************/

struct StackStruct {
  TNode* base;
  size_t size;
  size_t maxSize;
};

struct Node {
  TNode* next;
  uintptr_t valuePointer;
};

/***************** Declaration of Private Functions *****************/

static TNode*
mallocTNode(void);

static void
m_destroyRecursive(TNode* node, bool freeElements);

/***************** Definition of Public Functions *****************/

Stack
Stack_create(size_t size) {
  if (STACK_UNLIMITED_SIZE == size)
    return NULL;

  Stack stack = (Stack) malloc(sizeof(struct StackStruct));

  if (NULL == stack)
    return NULL;

  stack->base = mallocTNode();
  if (NULL == stack->base) {
    Stack_destroy(stack, 0);
    return NULL;
  }

  stack->base->valuePointer = 0;
  stack->base->next = NULL;

  stack->maxSize = size;
  stack->size = 0;

  return stack;
}

Stack
Stack_createUnlimited(void) {
  Stack stack = (Stack) malloc(sizeof(struct StackStruct));

  if (NULL == stack)
    return NULL;

  stack->base = mallocTNode();
  if (NULL == stack->base) {
    Stack_destroy(stack, 0);
    return NULL;
  }

  stack->base->valuePointer = 0;
  stack->base->next = NULL;

  stack->maxSize = STACK_UNLIMITED_SIZE;
  stack->size = 0;

  return stack;
}

void
Stack_destroy(Stack stack, bool freeElements) {
  if (NULL == stack)
    return;
  m_destroyRecursive(stack->base, freeElements);
  free(stack);
}

bool
Stack_push(const Stack stack, const void* valuePointer) {
  if (stack->size >= stack->maxSize && STACK_UNLIMITED_SIZE != stack->maxSize)
    return false;

  TNode* current = mallocTNode();

  if (NULL == current)
    return false;

  current->valuePointer = CAST(uintptr_t, valuePointer);
  current->next = stack->base;

  stack->base = current;
  stack->size++;

  return true;
}

void*
Stack_pop(const Stack stack, bool freeElement) {
  if (0 >= stack->size)
    return NULL;

  TNode* base = stack->base;
  stack->base = stack->base->next;
  uintptr_t valuePointer = base->valuePointer;

  free(base);
  stack->size--;

  return CAST_PTR(valuePointer);
}

void
Stack_print(const Stack stack) {
  TNode* current = stack->base;
  size_t i;
  printf("size: %lu\n", stack->size);

  for (i = 0; NULL != current->next; i++, current = current->next) {
    printf("[%lu] address: %p\tvaluePointer: %p\n",
           i,
           current,
           CAST_PTR(current->valuePointer));
  }
}

size_t
Stack_size(const Stack stack) {
  return stack->size;
}

void*
Stack_top(const Stack stack) {
  if (0 >= stack->size)
    return NULL;

  // Basically the same as Stack_pop, without decreasing
  // the size of this Stack.
  TNode* base = stack->base;
  stack->base = stack->base->next;
  uintptr_t valuePointer = base->valuePointer;

  return CAST_PTR(valuePointer);
}

/***************** Definition of Private Functions *****************/

static TNode*
mallocTNode(void) {
  return (TNode*) malloc(sizeof(TNode));
}

static void
m_destroyRecursive(TNode* node, bool freeElements) {
  if (NULL == node)
    return;
  m_destroyRecursive(node->next, freeElements);
  if (freeElements)
    free(CAST_PTR(node->valuePointer));
  free(node);
}
