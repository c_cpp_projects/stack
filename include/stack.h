#ifndef STACK_H
#define STACK_H

#include <stdbool.h>
#include <stddef.h>

/**
 * @brief Type alias for a pointer to the bytes of an element
 *        that is stored in a @c Stack .
 *
 *        This is not a typedef so 'const void*' does the right thing.
 */
#define Stack_ElementPtr void*

typedef struct StackStruct* Stack;

/**
 * @brief Create an empty new @c Stack.
 *
 * @param size The maximum size of your stack. This has to
 *             be greater than 0, else @b NULL gets returned.
 */
Stack
Stack_create(size_t size);

/**
 * @brief Create an empty new @c Stack, without a size constraint.
 */
Stack
Stack_createUnlimited(void);

/**
 * @brief Destroy @c stack and, if wanted, free its contents
 *        from memory.
 *
 * @param stack The Stack you want to destroy.
 * @param freeElements false if you do not want to apply free() on every valuePointer.
 *                     true if you want to apply free() on every valuePointer.
 */
void
Stack_destroy(Stack stack, bool freeElements);

/**
 * @brief Push a new element onto the Stack.
 *
 * @param stack The Stack you want to push a new element onto.
 * @param pElement The new element you want to push onto this Stack.
 *
 * @return true if the addition of valuePointer was unsuccessful,
 *         else false.
 */
bool
Stack_push(const Stack stack, const void* pElement);

/**
 * @brief Pop the element which was last added to this Stack.
 *
 * @param stack The Stack you want to pop an element from.
 * @param freeElement  false if you do not want to apply free() on the valuePointer.
 *                     true if you want to apply free() on the valuePointer.
 *
 * @return The valuePointer from the popped element, or NULL
 *         if freeElement is NOT equal to 0.
 */
void*
Stack_pop(const Stack stack, bool freeElement);

/**
 * @brief Print the contents of this Stack to the console.
 */
void
Stack_print(const Stack stack);

/**
 * @brief Get the current size of this Stack.
 *
 * @param stack The Stack from which you want to know the current size.
 *
 * @return This Stacks current size.
 */
size_t
Stack_size(const Stack stack);

/**
 * @brief Get the element that was last added to this Stack.
 *        This returns the same as Stack_pop, without removing
 *        the element from this Stack.
 *
 * @param stack The Stack you want to get the top element from.
 *
 * @return The top element from this Stack.
 */
void*
Stack_top(const Stack stack);

#endif  // STACK_H