#include "test_utils.h"

int
main(void) {
   Stack stack = Stack_create(2);

  TPerson pers1 = {1, "Manfred"};
  TPerson pers2 = {23, "Doni"};
  TPerson pers3 = {21221, "Lol"};

  printf("Success: %d\n", Stack_push(stack, &pers1));
  printf("Success: %d\n", Stack_push(stack, &pers2));
  printf("Success: %d\n", Stack_push(stack, &pers3));

  Stack_print(stack);

  Stack_destroy(stack, 0);

  return 0;
}