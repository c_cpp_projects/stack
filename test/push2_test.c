#include "test_utils.h"

int
main(void) {
  Stack stack = Stack_createUnlimited();

  Stack_print(stack);

  for (size_t i = 1; i < 501; i++)
    Stack_push(stack, (void*) i);

  Stack_print(stack);

  Stack_destroy(stack, 0);


  return 0;
}