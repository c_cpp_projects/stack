#include "test_utils.h"

int
main(void) {
  Stack stack = Stack_create(20);

  TPerson dani = {11231, "Doni"};
  TPerson niki = {13, "Paklhamma"};
  TPerson pers1 = {11, "Daniel"};

  Stack_push(stack, &dani);
  Stack_push(stack, &niki);
  Stack_push(stack, &pers1);

  Stack_print(stack);
  printf("\n");

  printPerson(&pers1);
  printPerson(&niki);
  printPerson(&dani);
  printf("\n");

  Stack_destroy(stack, 0);
  printf("After Destroy\n");

  Stack_print(stack);
  printf("\n");

  printPerson(&pers1);
  printPerson(&niki);
  printPerson(&dani);
  printf("\n");

  return 0;
}