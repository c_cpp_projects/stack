#include "test_utils.h"

int
main(void) {
  Stack stack = Stack_create(20);

  TPerson dani = {11231, "Doni"};
  TPerson niki = {13, "Paklhamma"};
  TPerson pers1 = {11, "Daniel"};

  Stack_print(stack);
  LINE_BR

  Stack_push(stack, &dani);
  Stack_push(stack, &niki);
  Stack_push(stack, &pers1);

  Stack_print(stack);
  LINE_BR

  TPerson* top = Stack_pop(stack, 0);
  printPerson(top);

  Stack_print(stack);
  LINE_BR

  top = Stack_pop(stack, 0);
  printPerson(top);

  Stack_print(stack);
  LINE_BR
  LINE_BR

  Stack_destroy(stack, 0);

  return 0;
}